package org.justsomehumans.p11.example_mod

import net.minecraft.item.Item
import net.minecraft.registry.Registries
import org.quiltmc.loader.api.ModContainer
import org.quiltmc.qkl.library.items.itemSettingsOf
import org.quiltmc.qkl.library.registry.registryScope
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

val EXAMPLE_ITEM: Item = Item(itemSettingsOf())

object ExampleMod : ModInitializer {
    val LOGGER: Logger = LoggerFactory.getLogger("Example Mod")

    override fun onInitialize(mod: ModContainer) {
        LOGGER.info("Hello Quilt world from {}!", mod.metadata()?.name())

        registryScope(mod.metadata().id()) {
            EXAMPLE_ITEM withPath "example_item" toRegistry Registries.ITEM
        }
    }

}
